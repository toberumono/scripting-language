package scriptingLanguage;

public class Type<T> extends lexer.Type<T> {
	
	public static final Type<Token> TOKEN = new Type<Token>("Token");
	
	/**
	 * A pre-created type that <i>must</i> be used to denote empty tokens (e.g. the end of a list)
	 */
	public static final Type<Object> EMPTY = new Type<Object>("Empty") {
		@Override
		public String valueToString(Object value) {
			return "";
		}
		
		@Override
		public int compareValues(Object value1, Object value2) {
			return 0;
		}
	};
	
	public Type(String name) {
		super(name);
	}
	
	public int getPrecedence() {
		return 0;
	}

	public boolean isLRAssociative() {
		return false;
	}
}
