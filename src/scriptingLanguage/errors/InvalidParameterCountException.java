package scriptingLanguage.errors;

public class InvalidParameterCountException extends InterpreterException {
	
	public InvalidParameterCountException(String message) {
		super(message);
	}
	
}
