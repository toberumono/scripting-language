package scriptingLanguage.errors;

public class InvalidAssignmentException extends InterpreterException {
	
	public InvalidAssignmentException(String message) {
		super(message);
	}
	
}
