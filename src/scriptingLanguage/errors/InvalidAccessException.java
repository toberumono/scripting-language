package scriptingLanguage.errors;

public class InvalidAccessException extends InterpreterException {
	
	public InvalidAccessException(String message) {
		super(message);
	}
	
}
