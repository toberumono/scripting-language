package scriptingLanguage.errors;

public class UndefinedResultException extends InterpreterException {
	
	public UndefinedResultException(String message) {
		super(message);
	}
	
}
