package scriptingLanguage.errors;

public class UnexpectedTypeException extends InterpreterException {

	public UnexpectedTypeException(String message) {
		super(message);
	}
	
}
