package scriptingLanguage.errors;

public class VariableNotFoundException extends InterpreterException {
	
	public VariableNotFoundException(String variable) {
		super(variable);
	}
	
}
