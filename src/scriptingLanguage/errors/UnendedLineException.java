package scriptingLanguage.errors;

public class UnendedLineException extends InterpreterException {
	
	public UnendedLineException(String message) {
		super(message);
	}
	
}
