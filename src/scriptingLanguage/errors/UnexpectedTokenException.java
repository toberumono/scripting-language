package scriptingLanguage.errors;

public class UnexpectedTokenException extends InterpreterException {
	
	public UnexpectedTokenException(String message) {
		//TODO add a/an
		super(message);
	}
}
