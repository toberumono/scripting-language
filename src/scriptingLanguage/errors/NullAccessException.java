package scriptingLanguage.errors;

public class NullAccessException extends InterpreterException {
	
	public NullAccessException(String message) {
		super(message);
	}
}
