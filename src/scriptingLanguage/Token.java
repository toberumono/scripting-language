package scriptingLanguage;

import lexer.abstractLexer.AbstractToken;

public class Token extends AbstractToken<Type<?>, Token> {
	
	public Token(Object car, Type<?> carType, Object cdr, Type<?> cdrType) {
		super(car, carType, cdr, cdrType);
	}
	
	public Token(Object car, Type<?> carType) {
		super(car, carType, null, Type.EMPTY);
	}
	
	public Token() {
		super(null, Type.EMPTY, null, Type.EMPTY);
	}
	
	@Override
	public Type<?> getCarType() {
		return (Type<?>) super.getCarType();
	}
	
	@Override
	public Type<?> getCdrType() {
		return (Type<?>) super.getCdrType();
	}
	
	@Override
	public Token singular() {
		return new Token(carType.clone(car), carType);
	}
	
	@Override
	public Token makeNewToken() {
		return new Token();
	}
	
	@Override
	protected Token clone(Token previous) {
		Token clone = new Token(carType.clone(car), carType, cdrType.clone(cdr), cdrType);
		clone.previous = previous;
		return clone;
	}
	
	@Override
	protected Type<?> getTokenType() {
		return Type.TOKEN;
	}
	
	/**
	 * Removes every token after this one from the list, without separating the subsequent tokens from each other and then
	 * returns the head of the new list.
	 * 
	 * @return the token after this one.
	 */
	public Token split() {
		if (cdr == null)
			return makeNewToken();
		Token result = (Token) cdr;
		cdr = null;
		cdrType = Type.EMPTY;
		result.previous = null;
		return result;
	}
}
