package scriptingLanguage;

public class Ternary implements Cloneable {
	private final Token ifTrue, ifFalse;
	
	public Ternary(Token ifTrue, Token ifFalse) {
		this.ifTrue = ifTrue;
		this.ifFalse = ifFalse;
	}
	
	public Token getTrue() {
		return ifTrue;
	}
	
	public Token getFalse() {
		return ifFalse;
	}
	
	@Override
	public Ternary clone() {
		return new Ternary(ifTrue.clone(), ifFalse.clone());
	}
}
