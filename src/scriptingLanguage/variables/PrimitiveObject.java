package scriptingLanguage.variables;

import scriptingLanguage.errors.UndefinedResultException;
import scriptingLanguage.errors.UnexpectedTypeException;
import scriptingLanguage.frames.AbstractFrame;

public class PrimitiveObject<T extends Comparable<T>> extends JavaObject<T> implements Comparable<T> {
	
	public PrimitiveObject(PrimitiveClass<T> type, T primitive) {
		super(type, primitive);
	}
	
	/**
	 * @return the type of this <tt>PrimitiveObject</tt>
	 */
	@Override
	public PrimitiveClass<T> getType() {
		return (PrimitiveClass<T>) super.getType();
	}
	
	@Override
	public PrimitiveObject<?> castData(AbstractClass<?> output, AbstractFrame frame) throws UnexpectedTypeException, UndefinedResultException {
		if (getType().equals(output))
			return this;
		if (getType() instanceof PrimitiveClass && output instanceof PrimitiveClass) {
			try {
				return ((PrimitiveClass<T>) output).makePrimitiveObject(getType().widen(getSource(), (PrimitiveClass<?>) output));
			}
			catch (UnexpectedTypeException e) {
				return ((PrimitiveClass<T>) output).makePrimitiveObject(getType().narrow(getSource(), (PrimitiveClass<?>) output));
			}
		}
		throw new UnexpectedTypeException("Instances of the " + getType().getName() + " type cannot be cast to " + output.getName());
	}
	
	@Override
	public int compareTo(T o) {
		if (o instanceof PrimitiveObject)
			return ((Comparable<T>) ((PrimitiveObject<?>) o).getSource()).compareTo(getSource());
		if (o instanceof Comparable)
			return ((Comparable<T>) o).compareTo(getSource());
		return 0;
	}
	
	@Override
	public boolean equals(Object o) {
		return getSource().equals(o instanceof Variable<?> ? ((Variable<?>) o).getSource() : o);
	}

	@Override
	public Variable<T> clone() {
		return new PrimitiveObject<T>(getType(), getSource());
	}
}
