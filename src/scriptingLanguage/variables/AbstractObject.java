package scriptingLanguage.variables;

import scriptingLanguage.errors.NullAccessException;
import scriptingLanguage.errors.UndefinedResultException;
import scriptingLanguage.errors.UnexpectedTypeException;
import scriptingLanguage.frames.AbstractFrame;

public abstract class AbstractObject<T> extends Variable<T> {
	
	public AbstractObject(AbstractClass<? extends T> type) {
		super(type, null);
	}
	
	public AbstractObject(AbstractClass<? extends T> type, T source) {
		super(type, source);
	}
	
	@Override
	public int extend(AbstractClass<?> type) throws NullAccessException {
		return getType().extend(type);
	}
	
	public abstract AbstractObject<?> castData(AbstractClass<?> output, AbstractFrame frame) throws UnexpectedTypeException, UndefinedResultException, NullAccessException;
}
