package scriptingLanguage.variables;

import java.util.ArrayList;

import scriptingLanguage.Interpreter;
import scriptingLanguage.Token;
import scriptingLanguage.Type;
import scriptingLanguage.errors.InterpreterException;
import scriptingLanguage.errors.NullAccessException;
import scriptingLanguage.errors.UndefinedResultException;
import scriptingLanguage.errors.UnexpectedTypeException;
import scriptingLanguage.frames.AbstractFrame;
import scriptingLanguage.frames.errors.FrameAccessException;

public abstract class AbstractMethodClass extends AbstractClass<Token> {
	private final ArrayList<AbstractClass<?>> parameters;
	private final AbstractClass<?> returnType;
	
	public AbstractMethodClass(String name, Token source, AbstractClass<?> returnType, AbstractFrame frame, Type<?> tokenType) throws UnexpectedTypeException, FrameAccessException {
		super(name, source, tokenType);
		this.parameters = new ArrayList<AbstractClass<?>>();
		this.returnType = returnType;
		if (!source.isNull()) {
			String type = source.getCar().toString();
			while (!(source = source.getNextToken()).isNull()) {
				if (source.getCarType().equals(Interpreter.callType))
					type = type + "." + (source = source.getNextToken()).getCar().toString();
				else {
					parameters.add(frame.getType(type));
					source = source.getNextToken().getNextToken();
				}
			}
			if (type.length() > 0)
				parameters.add(frame.getType(type));
		}
	}
	
	public AbstractMethodClass(String name, Token source, AbstractClass<?> returnType, ArrayList<AbstractClass<?>> parameters, Type<?> tokenType) {
		super(name, source, tokenType);
		this.returnType = returnType;
		this.parameters = parameters;
	}
	
	@Override
	public Token eval(AbstractClass<?> caller, Token parameters, AbstractFrame frame) throws ArrayIndexOutOfBoundsException, IllegalArgumentException, InterpreterException {
		throw new UndefinedResultException("Cannot instantiate a method from its class data.");
	}
	
	@Override
	public int extend(AbstractClass<?> type) throws NullAccessException {
		if (!(type instanceof AbstractMethodClass))
			return -1;
		int distance = 0;
		if ((distance = getReturnType().extend(((AbstractMethodClass) type).getReturnType())) == -1 || parameters.size() != ((AbstractMethodClass) type).parameters.size())
			return -1;
		for (int i = 0; i < parameters.size(); i++)
			if (parameters.get(i).extend(((AbstractMethodClass) type).parameters.get(i)) == -1)
				return -1;
		return distance;
	}
	
	/**
	 * @return the parameters
	 */
	public ArrayList<AbstractClass<?>> getParameters() {
		return parameters;
	}
	
	/**
	 * @return the returnType
	 */
	public AbstractClass<?> getReturnType() {
		return returnType;
	}
}
