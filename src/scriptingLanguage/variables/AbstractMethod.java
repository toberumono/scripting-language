package scriptingLanguage.variables;

import java.util.ArrayList;

import scriptingLanguage.Token;
import scriptingLanguage.errors.InterpreterException;
import scriptingLanguage.errors.InvalidAccessException;
import scriptingLanguage.errors.NullAccessException;
import scriptingLanguage.frames.AbstractFrame;

public abstract class AbstractMethod<T> extends Variable<T> {
	private AbstractFrame declared;
	
	AbstractMethod() {
		super(null, null);
		declared = null;
	}
	
	public AbstractMethod(AbstractClass<?> type, T source, AbstractFrame declared) {
		super(type, source);
		this.declared = declared;
	}
	
	public abstract Token eval(ArrayList<Token> args) throws ArrayIndexOutOfBoundsException, IllegalArgumentException, InterpreterException;
	
	/**
	 * @return the frame in which this method was declared
	 */
	public AbstractFrame getDeclared() {
		return declared;
	}
	
	/**
	 * This effectively amounts to forwarding to the return type of the method.
	 * 
	 * @throws NullAccessException
	 */
	@Override
	public int extend(AbstractClass<?> type) throws NullAccessException {
		return getType().extend(type);
	}
	
	@Override
	public void write(Variable<?> data) throws InvalidAccessException {
		super.write(data);
		declared = ((AbstractMethod<T>) data).declared;
	}
}
