package scriptingLanguage.variables;

public class Parameter {
	private final AbstractClass<?> type;
	private final String name;
	
	public Parameter(AbstractClass<?> type, String name) {
		this.type = type;
		this.name = name;
	}
	
	public final String getName() {
		return name;
	}
	
	public final AbstractClass<?> getType() {
		return type;
	}
}
