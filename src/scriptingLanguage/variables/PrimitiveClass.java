package scriptingLanguage.variables;

import scriptingLanguage.Token;
import scriptingLanguage.Type;
import scriptingLanguage.errors.InvalidAccessException;
import scriptingLanguage.errors.UndefinedResultException;
import scriptingLanguage.errors.UnexpectedTypeException;

public abstract class PrimitiveClass<T extends Comparable<T>> extends JavaClass<T> {

	public PrimitiveClass(String name, Class<T> source) {
		super(name, source);
	}
	
	public PrimitiveClass(String name, Class<T> source, Type<T> type) {
		super(name, source, type);
	}
	
	/**
	 * Widens a value of this type to the given type
	 * 
	 * @param value
	 *            the value
	 * @param otherType
	 *            the type
	 * @return
	 * @throws UnexpectedTypeException
	 */
	public abstract Object widen(Object value, PrimitiveClass<?> otherType) throws UnexpectedTypeException;
	
	/**
	 * Narrows a value of this type to the given type
	 * 
	 * @param value
	 *            the value
	 * @param otherType
	 *            the type
	 * @return
	 * @throws UnexpectedTypeException
	 */
	public abstract Object narrow(Object value, PrimitiveClass<?> otherType) throws UnexpectedTypeException;

	public Object cast(Object value, PrimitiveClass<?> type) throws UnexpectedTypeException {
		try {
			return widen(value, type);
		}
		catch (UnexpectedTypeException e) {
			return narrow(value, type);
		}
	}
	
	public abstract T add(Object value1, Object value2) throws UnexpectedTypeException, UndefinedResultException;
	
	public abstract T subtract(Object value1, Object value2) throws UnexpectedTypeException, UndefinedResultException;
	
	public abstract T multiply(Object value1, Object value2) throws UnexpectedTypeException, UndefinedResultException;
	
	public abstract T divide(Object value1, Object value2) throws UnexpectedTypeException, UndefinedResultException;
	
	public abstract T mod(Object value1, Object value2) throws UnexpectedTypeException, UndefinedResultException;
	
	public int compareValues(Object value1, Object value2) {
		return ((T) value1).compareTo((T) value2);
	}
	
	public PrimitiveObject<T> makePrimitiveObject(Object data) {
		return (PrimitiveObject<T>) eval(data).getCar();
	}
	
	@Override
	public Token eval(Object source) {
		return new Token(new PrimitiveObject<T>(this, (T) source), getTokenType());
	}
	
	@Override
	public void write(Variable<?> data) throws InvalidAccessException {
		throw new InvalidAccessException("Cannot overwrite a PrimitiveClass.");
	}
	
	@Override
	public Variable<Class<? extends T>> clone() {
		return this;
	}
	
	@Override
	public Token makePlaceholder() {
		return new Token(new PrimitiveObject<T>(this, null), getTokenType());
	}
}
