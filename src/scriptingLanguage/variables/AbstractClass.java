package scriptingLanguage.variables;

import scriptingLanguage.Interpreter;
import scriptingLanguage.Token;
import scriptingLanguage.Type;
import scriptingLanguage.errors.InvalidAccessException;

public abstract class AbstractClass<T> extends Variable<T> {
	private String name;
	private Type<?> tokenType;
	
	public AbstractClass(String name, T source, Type<?> tokenType) {
		super(Interpreter.voidClass, source);
		this.name = name;
		this.tokenType = tokenType;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof AbstractClass && ((AbstractClass<?>) o).getSource().getClass().isInstance(getSource()))
			return getName().equals(((AbstractClass<?>) o).getName());
		return false;
	}
	
	/**
	 * @return the tokenType for this AbstractClass
	 */
	public Type<?> getTokenType() {
		return tokenType;
	}
	
	@Override
	public String toString() {
		return getName();
	}
	
	@Override
	public void write(Variable<?> data) throws InvalidAccessException {
		super.write(data);
		AbstractClass<T> newData = (AbstractClass<T>) data;
		name = newData.name;
		tokenType = newData.tokenType;
	}
	
	public abstract Token makePlaceholder();
}
