package scriptingLanguage.variables;

import java.util.ArrayList;

import scriptingLanguage.Token;
import scriptingLanguage.Type;
import scriptingLanguage.errors.UnexpectedTypeException;
import scriptingLanguage.frames.AbstractFrame;
import scriptingLanguage.frames.errors.FrameAccessException;

public class InterpreterMethodClass extends AbstractMethodClass {
	
	public InterpreterMethodClass(String name, Token source, AbstractClass<?> returnType, AbstractFrame frame, Type<?> tokenType) throws UnexpectedTypeException, FrameAccessException {
		super(name, source, returnType, frame, tokenType);
	}
	
	public InterpreterMethodClass(String name, Token source, AbstractClass<?> returnType, ArrayList<AbstractClass<?>> parameters, Type<?> tokenType) {
		super(name, source, returnType, parameters, tokenType);
	}
	
	@Override
	public Token makePlaceholder() {
		return new Token(new InterpreterMethod(this), getTokenType());
	}
	
	@Override
	public Variable<Token> clone() {
		return new InterpreterMethodClass(getName(), getSource(), getReturnType(), getParameters(), getTokenType());
	}
	
}
