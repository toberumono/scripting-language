package scriptingLanguage.variables;

import scriptingLanguage.Interpreter;
import scriptingLanguage.Token;
import scriptingLanguage.Type;
import scriptingLanguage.errors.InterpreterException;
import scriptingLanguage.frames.AbstractFrame;

public class InterpreterConstructor extends InterpreterMethod {
	private final Token superParams;
	
	private InterpreterConstructor(InterpreterMethod base, Token method, Token superParams) {
		super((InterpreterMethodClass) base.getType(), base.getType(), method, base.getDeclared(), base.getParameters());
		this.superParams = superParams;
	}
	
	public static InterpreterConstructor init(InterpreterMethod base) {
		Token method = base.getSource(), superLine = new Token(), head = superLine;
		if (method.getCarType().equals(Interpreter.superType)) { //Then this is really easy because we already have the constructor
			method = method.getNextToken();
			Token params = (Token) method.getCar();
			if (!params.isNull())
				do {
					head = head.append(params.singular());
				} while (!(params = params.getNextToken()).getCarType().equals(Interpreter.endLineType) && !params.isNull());
			method = method.getNextToken();
		}
		else
			superLine = new Token();
		method.getLastToken().append(new Token("return", Interpreter.breakType, new Token("this", Interpreter.identifierType), Type.TOKEN));
		return new InterpreterConstructor(base, method, superLine);
		
	}
	
	public AbstractObject<?> initSuperclass(AbstractClass<?> caller, AbstractClass<?> superClass, Token parameters, AbstractFrame frame)
			throws ArrayIndexOutOfBoundsException, IllegalArgumentException, InterpreterException {
		return (AbstractObject<?>) superClass.eval(caller, superParams, frame).getCar();
	}
}
