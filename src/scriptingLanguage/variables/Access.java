package scriptingLanguage.variables;

import scriptingLanguage.errors.NullAccessException;
import scriptingLanguage.errors.VariableNotFoundException;

public enum Access {
	PUBLIC {
		@Override
		public boolean allowed(AbstractClass<?> accessor, AbstractClass<?> reference) {
			return true;
		}

		@Override
		public int intValue() {
			return 0;
		}
	},
	PROTECTED {
		@Override
		public boolean allowed(AbstractClass<?> accessor, AbstractClass<?> reference) throws VariableNotFoundException, NullAccessException {
			//TODO add packages
			return accessor.extend(reference) > -1;
		}

		@Override
		public int intValue() {
			return 1;
		}
	},
	DEFAULT { //Right now, this is basically the same as private
		@Override
		public boolean allowed(AbstractClass<?> accessor, AbstractClass<?> reference) {
			//TODO add packages
			return accessor.equals(reference);
		}

		@Override
		public int intValue() {
			return 2;
		}
	},
	PRIVATE {
		@Override
		public boolean allowed(AbstractClass<?> accessor, AbstractClass<?> reference) {
			return accessor.equals(reference);
		}

		@Override
		public int intValue() {
			return 3;
		}
	};
	
	public static Access getAccess(String name) {
		switch (name) {
			case "public":
				return PUBLIC;
			case "protected":
				return PROTECTED;
			default:
				return DEFAULT;
			case "private":
				return PRIVATE;
		}
	}
	
	public abstract boolean allowed(AbstractClass<?> accessor, AbstractClass<?> reference) throws VariableNotFoundException, NullAccessException;
	
	public abstract int intValue();
	
	@Override
	public String toString() {
		return super.toString().toLowerCase();
	}
	
	public static Access getAccess(AbstractClass<?> caller, AbstractClass<?> reference) throws VariableNotFoundException, NullAccessException {
		if (PRIVATE.allowed(caller, reference))
			return PRIVATE;
		if (DEFAULT.allowed(caller, reference))
			return DEFAULT;
		if (PROTECTED.allowed(caller, reference))
			return PROTECTED;
		return PUBLIC;
	}
}
