package scriptingLanguage.variables;

import lipstone.joshua.parser.types.BigDec;
import scriptingLanguage.Interpreter;
import scriptingLanguage.Token;
import scriptingLanguage.errors.InterpreterException;
import scriptingLanguage.errors.InvalidAccessException;
import scriptingLanguage.errors.NullAccessException;
import scriptingLanguage.frames.AbstractFrame;

public abstract class Variable<T> implements Cloneable {
	
	private T source;
	private final AbstractClass<?> type;
	
	public Variable(AbstractClass<?> type, T source) {
		this.type = type;
		this.source = source;
	}
	
	public abstract Token eval(AbstractClass<?> caller, Token parameters, AbstractFrame frame) throws ArrayIndexOutOfBoundsException, IllegalArgumentException, InterpreterException;
	
	/**
	 * @return the type of this <tt>Variable</tt>
	 */
	public AbstractClass<?> getType() {
		return type;
	}
	
	/**
	 * @return the source code of this method
	 */
	public T getSource() {
		return source;
	}
	
	/**
	 * Determines whether this <tt>Variable</tt> is of a type that extends the given type.
	 * 
	 * @param type
	 *            the type to check
	 * @return -1 if this class does not extend the given class. Otherwise, it returns a whole number indicating how many
	 *         "layers" there are in between the type of this <tt>Variable</tt> and the given type. That is, if the types are
	 *         the same, then the value is 0. If the given type is this variables direct superclass, it returns 1, if it is
	 *         the superclass's superclass, it returns 2, etc.
	 * @throws NullAccessException
	 */
	public abstract int extend(AbstractClass<?> type) throws NullAccessException;
	
	public static AbstractClass<?> determineType(Object data, AbstractFrame frame) throws InterpreterException {
		if (data == null)
			return Interpreter.voidClass;
		Class<?> d = (Class<?>) (data instanceof Class<?> ? data : data.getClass());
		//integer, string, character, double, character, boolean, complex, long, short, byte
		if (d.equals(Integer.class))
			return Interpreter.integerClass;
		if (d.equals(String.class))
			return Interpreter.stringClass;
		if (d.equals(Double.class))
			return Interpreter.doubleClass;
		if (d.equals(Character.class))
			return Interpreter.characterClass;
		if (d.equals(Boolean.class))
			return Interpreter.booleanClass;
		if (d.equals(BigDec.class))
			return Interpreter.complexClass;
		if (d.equals(Long.class))
			return Interpreter.longClass;
		if (d.equals(Short.class))
			return Interpreter.shortClass;
		if (d.equals(Byte.class))
			return Interpreter.byteClass;
		return frame.getType(d.getName());
	}
	
	public static Class<?> getClass(Object data) {
		Class<?> d = (Class<?>) (data instanceof Class<?> ? data : data.getClass());
		if (d.equals(Integer.class))
			return int.class;
		if (d.equals(Double.class))
			return double.class;
		if (d.equals(Character.class))
			return char.class;
		if (d.equals(Boolean.class))
			return boolean.class;
		if (d.equals(Long.class))
			return long.class;
		if (d.equals(Short.class))
			return short.class;
		if (d.equals(Byte.class))
			return byte.class;
		return d;
	}
	
	public void writeSource(Object source) {
		this.source = (T) source;
	}
	
	public void write(Variable<?> data) throws InvalidAccessException {
		this.source = (T) data.source;
	}
	
	@Override
	public abstract Variable<T> clone();
}
