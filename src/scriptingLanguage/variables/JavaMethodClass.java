package scriptingLanguage.variables;

import java.util.ArrayList;

import scriptingLanguage.Token;
import scriptingLanguage.Type;
import scriptingLanguage.errors.UnexpectedTypeException;
import scriptingLanguage.frames.AbstractFrame;
import scriptingLanguage.frames.errors.FrameAccessException;

public class JavaMethodClass extends AbstractMethodClass {
	
	public JavaMethodClass(String name, Token source, AbstractClass<?> returnType, AbstractFrame frame, Type<?> tokenType) throws UnexpectedTypeException, FrameAccessException {
		super(name, source, returnType, frame, tokenType);
	}
	
	public JavaMethodClass(String name, Token source, AbstractClass<?> returnType, ArrayList<AbstractClass<?>> parameters, Type<?> tokenType) {
		super(name, source, returnType, parameters, tokenType);
	}
	
	public static JavaMethodClass make(AbstractClass<?> returnType, AbstractClass<?>... parameters) {
		String name = returnType.getName() + ":{";
		int i = 0;
		ArrayList<AbstractClass<?>> params = new ArrayList<>();
		for (; i < parameters.length - 1; i++) {
			params.add(parameters[i]);
			name = name + parameters[i] + ", ";
		}
		params.add(parameters[i]);
		name = name + parameters[i] + "}";
		return new JavaMethodClass(name, null, returnType, params, new Type<Object>(name));
	}
	
	@Override
	public Token makePlaceholder() {
		return new Token(new JavaMethod(this), getTokenType());
	}
	
	@Override
	public Variable<Token> clone() {
		return new JavaMethodClass(getName(), getSource(), getReturnType(), getParameters(), getTokenType());
	}
	
}
