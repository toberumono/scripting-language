package scriptingLanguage.frames;

import lipstone.joshua.customStructures.tuples.Pair;
import scriptingLanguage.Interpreter;
import scriptingLanguage.Token;
import scriptingLanguage.errors.InterpreterException;
import scriptingLanguage.errors.InvalidAssignmentException;
import scriptingLanguage.errors.VariableNotFoundException;
import scriptingLanguage.variables.AbstractClass;
import scriptingLanguage.variables.Variable;

public class ObjectFrame extends Frame {
	private final AbstractFrame staticFrame;
	
	public ObjectFrame(AbstractFrame previous, Interpreter interpreter, AbstractFrame staticFrame) {
		super(previous, interpreter);
		this.staticFrame = staticFrame;
	}
	
	@Override
	public boolean writeVariable(String variable, Token value, boolean isNew) throws InterpreterException {
		if (isNew) {
			if (variables.containsKey(variable))
				throw new InvalidAssignmentException("The a variable called " + variable + " already exists in this frame.");
			variables.put(variable, new Pair<AbstractClass<?>, Token>(((Variable<?>) value.getCar()).getType(), value));
			return true;
		}
		try {
			return super.writeVariable(variable, value, false);
		}
		catch (VariableNotFoundException e) {
			//else if (previous != null && !(previous instanceof RootFrame))
			//return previous.writeVariable(variable, value, false); //Cannot be a first assignment
			//else
			return staticFrame.writeVariable(variable, value, false); //Cannot be a first assignment
		}
	}
	
	@Override
	public Token readVariable(String variable) throws InterpreterException {
		try {
			return super.readVariable(variable);
		}
		catch (VariableNotFoundException e) {
			return staticFrame.readVariable(variable);
		}
	}
}
