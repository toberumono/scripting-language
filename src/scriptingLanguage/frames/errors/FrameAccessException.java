package scriptingLanguage.frames.errors;

import scriptingLanguage.errors.InterpreterException;

public class FrameAccessException extends InterpreterException {
	
	public FrameAccessException(String message) {
		super(message);
	}
	
}
