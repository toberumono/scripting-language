package scriptingLanguage.frames;

import java.lang.reflect.Method;
import java.util.LinkedHashMap;

import lipstone.joshua.customStructures.tuples.Pair;
import lipstone.joshua.parser.Parser;
import scriptingLanguage.Interpreter;
import scriptingLanguage.Token;
import scriptingLanguage.variables.AbstractClass;
import scriptingLanguage.variables.JavaClass;
import scriptingLanguage.variables.JavaMethod;
import scriptingLanguage.variables.JavaMethodClass;
import scriptingLanguage.variables.JavaObject;

public class InterpreterRoot extends RootFrame {
	
	public InterpreterRoot(Parser parser, Interpreter interpreter) {
		super(null, interpreter);
		variables = new LinkedHashMap<>();
		JavaClass<System> systemClass = new JavaClass<>("System", System.class);
		variables.put("System", new Pair<AbstractClass<?>, Token>(systemClass, new Token(systemClass, Interpreter.variableType)));
		JavaClass<Parser> parserClass = new JavaClass<>("Parser", Parser.class);
		variables.put("parser", new Pair<AbstractClass<?>, Token>(parserClass, new Token(new JavaObject<>(parserClass, parser), Interpreter.variableType)));
		try {
			JavaClass<Method> methodClass = new JavaClass<>("Method", Method.class);
			variables.put("print:{Object}", new Pair<AbstractClass<?>, Token>(methodClass,
					new Token(new JavaMethod(JavaMethodClass.make(Interpreter.voidClass, Interpreter.ObjectClass), System.out.getClass().getMethod("print", Object.class), System.out, this), methodClass.getTokenType())));
			variables.put("println:{Object}", new Pair<AbstractClass<?>, Token>(methodClass,
					new Token(new JavaMethod(JavaMethodClass.make(Interpreter.voidClass, Interpreter.ObjectClass), System.out.getClass().getMethod("println", Object.class), System.out, this), methodClass.getTokenType())));
		}
		catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
	}
}
